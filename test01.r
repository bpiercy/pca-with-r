# Heavily adapted from gastonsanchez.com, PCA_with_R.pdf, 2014
# PCA with R
# Goal: reduce the #variables in a dataset while retaining as much data
# variation as possible.

#load package RCurl
#library(RCurl)

# simple "cars2004" dataset: 24 rows
# name, cylinders, horsepower, speed, weight, width, length
# name = string, all other vals = integer

cars2004 =read.csv("cars2004.csv", row.names = 1, header = TRUE)

# summary statistics
cars_stats =data.frame(
  Minimum =apply(cars2004, 2, min),
  Maximum =apply(cars2004, 2, max),
  Mean =apply(cars2004, 2, mean),
  Std_Dev =apply(cars2004, 2, sd))

print(cars_stats, print.gap = 3)

# Stars plot
# dissimilarities between the car models.
stars(
  cars2004, 
  labels = abbreviate(
    rownames(cars2004), 6),
  nrow = 4, 
  key.loc = c(8, 11.2))

abline(h = 9.85, col = "gray90")

# scatter plot:
pairs(cars2004)

# correlations among the varaibles
# notice that all correlations are positive
as.dist(round(cor(cars2004), 3))

# PCA in R:
# function # package
# =========================
# prcomp() # stats
# princomp() # stats
# PCA() # FactoMineR
# dudi.pca() # ade4
# acp() # amap
# nipals() # plsdepot
# rda() # vegan

# Eigenvalues (amount of variability captured by ea principal component)
# Scores ("PCs") (coordinates to graph objects in lower-D space)
# Loadings (which vars characterize ea principal component)

# prcomp with scale. = TRUE ==> PCA calc'd on std data (mean = 0, var = 1)
cars_prcomp = prcomp(cars2004, scale. = TRUE)
names(cars_prcomp)
cars_prcomp$sdev^2

# scores
round(head(cars_prcomp$x, 5), 2)

# loadings
round(head(cars_prcomp$rotation, 5), 2)

# PCA with princomp()
cars_princomp =princomp(cars2004, cor = TRUE)

names(cars_princomp)
# eigenvalues
cars_princomp$sdev^2

# PCA with princomp() cont'd
# scores
round(head(cars_princomp$scores, 5), 3)
# loadings
round(head(unclass(cars_princomp$loadings), 5), 3)

# A richer & nicer PCA() with FactoMineR

library(FactoMineR)

cars_pca = PCA(cars2004, graph=FALSE)
cars_pca # what methods are available in PCA?

# How many PCs should I retain?
# No universal criterion. Look at eigenvalues for %variance
# captured by each dimension.

cars_pca$eig # returns eigenvalue, %variance, cume %variance
barplot(cars_pca$eig[,"eigenvalue"], border=NA, col="gray80", names.arg=rownames(cars_pca$eig))

# to see how each PC is characterized, check loadings or correlations
# between variables and the PCs.

round(cars_pca$var$coord[,1:2],4)

# results: PC1 = pos.correlated with all variables; PC2 opposes
# weight & length against cylinders, horsepower, speed, weight.

# correlations circle:
# the closer an arrow to the circumference of the circle, the better its
# representations on the given axes.

plot(cars_pca, choix = "var")

# Variables' influence on each PC:
# If all vars contributed uniformly, each contribution would be 1/6 ~ 16.6%
print(rbind(cars_pca$var$contrib, TOTAL = colSums(cars_pca$var$contrib)), print.gap = 3)

library(RColorBrewer)
colpal = brewer.pal(n=5, name="Blues")[5:1]

barplot(t(cars_pca$var$contrib), biside=TRUE, border=NA, ylim=c(0,90), col=colpal, legend.text=colnames(cars_pca$var$contrib), args.legend = list(x="top", ncol=5, bty='n'))
abline(h=16, col="#ff572255", lwd=2)

# PC scores:
# we can use scores as scatterplot object coordinates

print(round(cars_pca$ind$coord[,1:2], 3), print.gap = 3)

# default object plot
plot(cars_pca, choix = "ind")

# alternative plot with ggplot2
library(ggplot2)
cars_pca_obs = data.frame(cars_pca$ind$coord[,1:3])

ggplot(cars_pca_obs, aes(x=Dim.1, y=Dim.2, label=rownames(cars2004))) +
  geom_hline(yintercept = 0, color = "gray70") +
  geom_vline(xintercept = 0, color = "gray70") +
  geom_point(color = "#55555544", size = 5) +
  geom_text(alpha = 0.55, size = 4) +
  xlab("PC1") +
  ylab("PC2") +
  xlim(-5, 6) +
  ggtitle("PCA plot of observations")

# objects' contributions (influence) to PCs:
# 1st 2 dimensions

print(round(cars_pca$ind$contrib[,1:2],3), print.gap=3)

op = par(mfrow = c(2,1))

# PC1 contributions
barplot(cars_pca$ind$contrib[,1], border=NA, las=2, names.arg = abbreviate(rownames(cars2004), 8), cex.names=0.8)
title("Object Contibutions on PC1", cex.main=0.9)
abline(h=4.16, col="gray50")

# PC2 contributions
barplot(cars_pca$ind$contrib[,2], border=NA, las=2, names.arg = abbreviate(rownames(cars2004), 8), cex.names=0.8)
title("Object Contibutions on PC2", cex.main=0.9)
abline(h=4.16, col="gray50")

par(op)

# PCA & clustering
# is there a typology? if so, how to cluster?
# let's try hierarchical clustering first.

cars_clustering = hclust(dist(cars_pca$ind$coord), method="ward")
plot(cars_clustering, xlab="", sub="")

# PC plot with clustering partition

# get k=3 cluster, add it to scores' data frame, then plot

cars_clusters = cutree(cars_clustering, k=3)
cars_pca_obs$cluster = as.factor(cars_clusters)

ggplot(cars_pca_obs,aes(x=Dim.1, y=Dim.2, label=rownames(cars2004))) +
  geom_hline(yintercept = 0, color = "gray70") +
  geom_vline(xintercept = 0, color = "gray70") +
  geom_point(aes(color = cluster), alpha = 0.55, size = 3) +
  geom_text(aes(color = cluster), alpha = 0.55, size = 4) +
  xlab("PC1") +
  ylab("PC2") +
  xlim(-5, 6) +
  ggtitle("PCA plot of observations")